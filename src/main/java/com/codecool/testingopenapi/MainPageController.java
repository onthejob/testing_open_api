package com.codecool.testingopenapi;


import com.codecool.testingopenapi.api.TestOpenApiApi;
import com.codecool.testingopenapi.model.ApiTestModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainPageController implements TestOpenApiApi {


    @Override
    public ResponseEntity<ApiTestModel> getTestResult() {
        return ResponseEntity.ok(ApiTestModel.builder().message("Finally I'm workin").age(1).build());
    }
}
