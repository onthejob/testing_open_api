package com.codecool.testingopenapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingOpenApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestingOpenApiApplication.class, args);
    }

}
